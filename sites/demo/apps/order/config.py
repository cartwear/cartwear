from cartwear.apps.order import config


class OrderConfig(config.OrderConfig):
    name = 'apps.order'
