============
Sandbox site
============

This site is deployed there:

http://latest.cartwearcommerce.com
-------------------------------

This site is used for testing the vanilla Cartwear functionality, that is, Cartwear
without any customisation.  It is not a demo site for clients or potential Cartwear
users.  It is built automatically from the HEAD of the master branch every 20
minutes.

