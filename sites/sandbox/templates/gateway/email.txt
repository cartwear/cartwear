{% load i18n %}
{% blocktrans %}
A dashboard user has been created for you with credentials:

- Email: {{ email }}
- Password: {{ password }}

Sign into the dashboard here:
http://latest.cartwearcommerce.com/dashboard/

Feel free to play around with the dashboard, although please don't deface the
public-facing pages.

Best wishes,
Cartwear Team
{% endblocktrans %}
