from django.test import TestCase
from django.template import loader, base, Context

import cartwear


class TestCartwearCoreAppsList(TestCase):

    def test_includes_cartwear_itself(self):
        core_apps = cartwear.CARTWEAR_CORE_APPS
        self.assertTrue('cartwear' in core_apps)

    def test_can_be_retrieved_through_fn(self):
        core_apps = cartwear.get_core_apps()
        self.assertTrue('cartwear' in core_apps)

    def test_can_be_retrieved_with_overrides(self):
        apps = cartwear.get_core_apps(overrides=['apps.shipping'])
        self.assertTrue('apps.shipping' in apps)
        self.assertTrue('cartwear.apps.shipping' not in apps)

    def test_raises_exception_for_string_arg(self):
        with self.assertRaises(ValueError):
            cartwear.get_core_apps('forks.catalogue')


class TestCartwearTemplateSettings(TestCase):
    """
    Cartwear's CARTWEAR_MAIN_TEMPLATE_DIR setting
    """
    def test_allows_a_template_to_be_accessed_via_two_paths(self):
        paths = ['base.html', 'cartwear/base.html']
        for path in paths:
            try:
                loader.get_template(path)
            except base.TemplateDoesNotExist:
                self.fail("Template %s should exist" % path)

    def test_allows_a_template_to_be_customized(self):
        path = 'base.html'
        template = loader.get_template(path)
        rendered_template = template.render(Context())
        self.assertIn('Cartwear Test Shop', rendered_template)

    def test_default_cartwear_templates_are_accessible(self):
        path = 'cartwear/base.html'
        template = loader.get_template(path)
        rendered_template = template.render(Context())
        self.assertNotIn('Cartwear Test Shop', rendered_template)
