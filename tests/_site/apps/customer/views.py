from cartwear.apps.customer.views import AccountSummaryView as CartwearAccountSummaryView


class AccountSummaryView(CartwearAccountSummaryView):
    # just here to test import in loading_tests:ClassLoadingWithLocalOverrideTests
    pass
