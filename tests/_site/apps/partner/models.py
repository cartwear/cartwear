from django.db import models

from cartwear.apps.partner import abstract_models


class StockRecord(abstract_models.AbstractStockRecord):
    # Dummy additional field
    offer_name = models.CharField(max_length=128, null=True, blank=True)


from cartwear.apps.partner.models import *
