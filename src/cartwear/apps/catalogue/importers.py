import os
import zlib
import tarfile
import zipfile
import tempfile
import shutil
from PIL import Image

from decimal import Decimal as D
from datetime import datetime

from openpyxl import load_workbook

from django.db.transaction import atomic
from django.utils.translation import ugettext_lazy as _
from django.core.files import File
from django.core.exceptions import FieldError

from cartwear.apps.catalogue.categories import create_from_breadcrumbs
from cartwear.core.loading import get_class, get_classes
from cartwear.core.compat import UnicodeCSVReader
from cartwear.core.loading import get_model
from cartwear.apps.catalogue.exceptions import (
    ImageImportError, IdenticalImageError, InvalidImageArchive)

ImportingError = get_class('partner.exceptions', 'ImportingError')
Partner, StockRecord = get_classes('partner.models',
                            ['Partner','StockRecord'])
ProductClass, Product, Category, ProductCategory, ProductImage = get_classes(
    'catalogue.models', ('ProductClass', 'Product', 'Category',
                         'ProductCategory','ProductImage'))


class BulkCatalogueImporter(object):
    """
    Bulk catalgue product importer.
    Used by the sellers to put all the items into the marketplace.
    """
    validate_title = False
    attribute_codes = []

    def get_products(self, product_type):
        product_sheet = self.wb[product_type]
        return product_sheet

    @atomic
    def handle(self, filepath):
        self.wb = load_workbook(filename = filepath)
        sheets = self.wb.worksheets
        welcome_sheet = self.wb['Welcome']
        version = welcome_sheet['A26'].value
        product_type = welcome_sheet['A2'].value

        product_class = ProductClass.objects.get(
            name=product_type)

        # if version == 0.1:
        #     try:
        product_type = welcome_sheet['A2'].value
        seller_id = welcome_sheet['B2'].value
        seller_secret =  welcome_sheet['C2'].value
        seller_email = welcome_sheet['D2'].value

        product_sheet = self.get_products(product_type)
        partner = Partner.objects.get(uid=seller_id)
        validate_title = False

        for row in product_sheet.iter_rows(row_offset=0):
            raw_row = []
            for cell in row: raw_row.append(cell.value)
            self.add_product(product_class, raw_row, partner)

    def add_product(self, product_class, raw_row, partner):
        (Type, UPC, Title, Description, Category, SKU, RetailPrice, Price, Stock) = raw_row[0:9]

        if self.validate_title:
            try:
                # Create product
                is_child = Type.lower() == 'variant'
                is_parent = Type.lower() == 'group'
            except AttributeError:
                return False

            if UPC:
                try:
                    product = Product.objects.get(upc=UPC)
                except Product.DoesNotExist:
                    product = Product(upc=UPC)
            else:
                product = Product()

            if is_child:
                product.structure = Product.CHILD
                # Assign parent for children
                product.parent = self.parent
            elif is_parent:
                product.structure = Product.PARENT
            else:
                product.structure = Product.STANDALONE

            if not product.is_child:
                product.title = Title
                product.description = Description
                product.product_class = product_class

            # Attributes
            if not product.is_parent:
                for code, value in zip(self.attribute_codes, raw_row[10:]):
                    # Need to check if the attribute requires an Option instance
                    print code
                    attr = product_class.attributes.get(
                        code=code)
                    if attr.is_option:
                        value = attr.option_group.options.get(option=value)
                    if attr.type == 'date':
                        value = datetime.strptime(value, "%d/%m/%y").date()
                    setattr(product.attr, code, value)

            product.save()

            # Save a reference to last parent product
            if is_parent:
                self.parent = product

            # Category information
            if Category:
                categories = [x.strip() for x in Category.split('|')]
                for cat in categories:
                    leaf = create_from_breadcrumbs(cat)
                    ProductCategory.objects.get_or_create(
                        product=product, category=leaf)

            # Stock record
            if partner:
                try:
                    record = StockRecord.objects.get(product=product)
                except StockRecord.DoesNotExist:
                    record = StockRecord(product=product)
                record.partner_id = partner.id
                record.partner_sku = str(SKU)
                print "SKU:" + record.partner_sku
                record.price_excl_tax = D(Price)
                record.price_retail = D(RetailPrice)
                if Stock != 'NULL':
                    record.num_in_stock = Stock
                record.save()

        elif (eval(Type),eval(UPC),eval(Title),eval(Description),\
            eval(Category),eval(SKU),eval(RetailPrice),eval(Price),eval(Stock)) \
            == ('Type','UPC','Title','Description','Category', 'SKU', 'RetailPrice', 'Price','Stock'):
            self.validate_title = True
            self.attribute_codes = raw_row[10:]


class BulkImageImporter(object):

    # This is for importing bulk images which matches the image filename
    # and the SKU with partners SKU, It even checks if there are duplicate.
    allowed_extensions = ['.jpeg', '.jpg', '.gif', '.png']

    def __init__(self, logger, partner):
        self.logger = logger
        self.partner = partner

    @atomic  # noqa (too complex (10))
    def handle(self, dirname):
        stats = {
            'num_processed': 0,
            'num_skipped': 0,
            'num_invalid': 0}
        chrt = ["-","_"]
        image_dir, filenames = self._get_image_files(dirname)
        if image_dir:
            for filename in filenames:
                try:
                    lookup_value \
                        = self._get_lookup_value_from_filename(filename)
                    # Spliting the lookup_value if it has a separators like _ or -.
                    for c in chrt: lookup_value = lookup_value.split(c)[0]
                    self._process_image(image_dir, filename, lookup_value)
                    stats['num_processed'] += 1
                except Product.MultipleObjectsReturned:
                    self.logger.warning("Multiple products matching %s='%s',"
                                        " skipping"
                                        % (filename, lookup_value))
                    stats['num_skipped'] += 1
                except Product.DoesNotExist:
                    self.logger.warning("No item matching %s='%s'"
                                        % (filename, lookup_value))
                    stats['num_skipped'] += 1
                except IdenticalImageError:
                    self.logger.warning("Identical image already exists for"
                                        " %s='%s', skipping"
                                        % (filename, lookup_value))
                    stats['num_skipped'] += 1
                except IOError as e:
                    stats['num_invalid'] += 1
                    raise ImageImportError(_('%(filename)s is not a valid'
                                             ' image (%(error)s)')
                                           % {'filename': filename,
                                              'error': e})
                except FieldError as e:
                    raise ImageImportError(e)
            if image_dir != dirname:
                shutil.rmtree(image_dir)
        else:
            raise InvalidImageArchive(_('%s is not a valid image archive')
                                      % dirname)
        self.logger.info("Finished image import: %(num_processed)d imported,"
                         " %(num_skipped)d skipped" % stats)

    def _get_image_files(self, dirname):
        filenames = []
        image_dir = self._extract_images(dirname)
        if image_dir:
            for filename in os.listdir(image_dir):
                ext = os.path.splitext(filename)[1]
                if os.path.isfile(os.path.join(image_dir, filename)) \
                        and ext in self.allowed_extensions:
                    filenames.append(filename)
        return image_dir, filenames

    def _extract_images(self, dirname):
        '''
        Returns path to directory containing images in dirname if successful.
        Returns empty string if dirname does not exist, or could not be opened.
        Assumes that if dirname is a directory, then it contains images.
        If dirname is an archive (tar/zip file) then the path returned is to a
        temporary directory that should be deleted when no longer required.
        '''
        if os.path.isdir(dirname):
            return dirname

        ext = os.path.splitext(dirname)[1]
        if ext in ['.gz', '.tar']:
            image_dir = tempfile.mkdtemp()
            try:
                tar_file = tarfile.open(dirname)
                tar_file.extractall(image_dir)
                tar_file.close()
                return image_dir
            except (tarfile.TarError, zlib.error):
                return ""
        elif ext == '.zip':
            image_dir = tempfile.mkdtemp()
            try:
                zip_file = zipfile.ZipFile(dirname)
                zip_file.extractall(image_dir)
                zip_file.close()
                return image_dir
            except (zlib.error, zipfile.BadZipfile, zipfile.LargeZipFile):
                return ""
        # unknown archive - perhaps this should be treated differently
        return ""

    def _process_image(self, dirname, filename, lookup_value):
        file_path = os.path.join(dirname, filename)
        trial_image = Image.open(file_path)
        trial_image.verify()
        item = StockRecord.objects.get(partner_sku=lookup_value,partner=self.partner).product
        new_data = open(file_path, 'rb').read()
        next_index = 0
        for existing in item.images.all():
            next_index = existing.display_order + 1
            try:
                if new_data == existing.original.read():
                    raise IdenticalImageError()
            except IOError:
                # File probably doesn't exist
                existing.delete()

        new_file = File(open(file_path, 'rb'))
        im = ProductImage(product=item, display_order=next_index)
        im.original.save(filename, new_file, save=False)
        im.save()
        self.logger.debug('Image added to "%s"' % item)

    def _get_lookup_value_from_filename(self, filename):
        filename = os.path.splitext(filename)[0]
        return filename.split("-")[0]
