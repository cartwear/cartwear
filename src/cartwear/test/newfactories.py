import warnings

from cartwear.test.factories.address import *  # noqa
from cartwear.test.factories.basket import *  # noqa
from cartwear.test.factories.catalogue import *  # noqa
from cartwear.test.factories.contrib import *  # noqa
from cartwear.test.factories.customer import *  # noqa
from cartwear.test.factories.offer import *  # noqa
from cartwear.test.factories.order import *  # noqa
from cartwear.test.factories.partner import *  # noqa
from cartwear.test.factories.payment import *  # noqa
from cartwear.test.factories.utils import *  # noqa
from cartwear.test.factories.voucher import *  # noqa


message = (
    "Module '%s' is deprecated and will be removed in the next major version "
    "of django-cartwear"
) % __name__

warnings.warn(message, PendingDeprecationWarning)
