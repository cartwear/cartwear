import os

# Use 'dev', 'beta', or 'final' as the 4th element to indicate release type.
VERSION = (1, 1, 0, 'dev')


def get_short_version():
    return '%s.%s' % (VERSION[0], VERSION[1])


def get_version():
    version = '%s.%s' % (VERSION[0], VERSION[1])
    # Append 3rd digit if > 0
    if VERSION[2]:
        version = '%s.%s' % (version, VERSION[2])
    elif VERSION[3] != 'final':
        version = '%s %s' % (version, VERSION[3])
        if len(VERSION) == 5:
            version = '%s %s' % (version, VERSION[4])
    return version


# Cheeky setting that allows each template to be accessible by two paths.
# Eg: the template 'cartwear/templates/cartwear/base.html' can be accessed via both
# 'base.html' and 'cartwear/base.html'.  This allows Cartwear's templates to be
# extended by templates with the same filename
CARTWEAR_MAIN_TEMPLATE_DIR = os.path.join(
    os.path.dirname(os.path.abspath(__file__)), 'templates/cartwear')

CARTWEAR_CORE_APPS = [
    'cartwear',
    'cartwear.apps.analytics',
    'cartwear.apps.checkout',
    'cartwear.apps.address',
    'cartwear.apps.shipping',
    'cartwear.apps.catalogue',
    'cartwear.apps.catalogue.reviews',
    'cartwear.apps.partner',
    'cartwear.apps.basket',
    'cartwear.apps.payment',
    'cartwear.apps.offer',
    'cartwear.apps.order',
    'cartwear.apps.customer',
    'cartwear.apps.promotions',
    'cartwear.apps.search',
    'cartwear.apps.voucher',
    'cartwear.apps.wishlists',
    'cartwear.apps.dashboard',
    'cartwear.apps.dashboard.reports',
    'cartwear.apps.dashboard.users',
    'cartwear.apps.dashboard.orders',
    'cartwear.apps.dashboard.promotions',
    'cartwear.apps.dashboard.catalogue',
    'cartwear.apps.dashboard.offers',
    'cartwear.apps.dashboard.partners',
    'cartwear.apps.dashboard.pages',
    'cartwear.apps.dashboard.ranges',
    'cartwear.apps.dashboard.reviews',
    'cartwear.apps.dashboard.vouchers',
    'cartwear.apps.dashboard.communications',
    'cartwear.apps.dashboard.shipping',
    # 3rd-party apps that cartwear depends on
    'haystack',
    'treebeard',
    'sorl.thumbnail',
    'django_tables2',
    'taggit'
]


def get_core_apps(overrides=None):
    """
    Return a list of cartwear's apps amended with any passed overrides
    """
    if not overrides:
        return CARTWEAR_CORE_APPS

    # Conservative import to ensure that this file can be loaded
    # without the presence Django.
    from django.utils import six
    if isinstance(overrides, six.string_types):
        raise ValueError(
            "get_core_apps expects a list or tuple of apps "
            "to override")

    def get_app_label(app_label, overrides):
        pattern = app_label.replace('cartwear.apps.', '')
        for override in overrides:
            if override.endswith(pattern):
                if 'dashboard' in override and 'dashboard' not in pattern:
                    continue
                return override
        return app_label

    apps = []
    for app_label in CARTWEAR_CORE_APPS:
        apps.append(get_app_label(app_label, overrides))
    return apps
