================================
How to change Cartwear's appearance
================================

This is a guide for Front-End Developers (FEDs) working on Cartwear projects, not
on Cartwear itself.  It is written with Tangent's FED team in mind but should be
more generally useful for anyone trying to customise Cartwear and looking for the
right approach.

Overview
========

Cartwear ships with a set of HTML templates and a collection of static files
(eg images, javascript).  Cartwear's default CSS is generated from LESS
files.

Templates
---------

Cartwear's default templates use the mark-up conventions from the Bootstrap
project. Classes for styling should be separate from classes used for
Javascript. The latter must be prefixed with ``js-``, and using data attributes
is often preferable.

Frontend vs. Dashboard
----------------------

The frontend and dashboard are intentionally kept very separate. They
incidentally both use Bootstrap, but may be updated individually.
The frontend is based on Bootstrap's LESS files and ties it together with
Cartwear-specific styling in ``styles.less``.

On the other hand, ``dashboard.less`` just contains a few customisations that
are included alongside a copy of stock Bootstrap CSS - and at the time of
writing, using a different Bootstrap version.

LESS/CSS
--------

By default, CSS files compiled from their LESS sources are used rather than the
LESS ones.  To use Less directly, set ``USE_LESS = True`` in your settings file.
You will also need to ensure that the ``lessc`` executable is installed and is
configured using a setting like::

    COMPRESS_PRECOMPILERS = (
        ('text/less', 'lessc {infile} {outfile}'),
    )

A few other CSS files are used to provide styles for javascript libraries.

Using offline compression
~~~~~~~~~~~~~~~~~~~~~~~~~

Django compressor also provides a way of running offline compression which can
be used during deployment to automatically generate CSS files from your LESS
files. To make sure that compressor is obeying the ``USE_LESS`` setting and
is not trying to compress CSS files that are not available, the setting has to
be passed into the ``COMPRESS_OFFLINE_CONTEXT``. You should add something like
this to your settings file::

    COMPRESS_OFFLINE_CONTEXT = {
        # this is the only default value from compressor itself
        'STATIC_URL': STATIC_URL,
        'use_less': USE_LESS,
    }


Javascript
----------

Cartwear uses javascript for progressive enhancements. This guide used to document
exact versions, but quickly became outdated. It is recommended to inspect
``layout.html`` and ``dashboard/layout.html`` for what is currently included.

Customisation
=============

Customising templates
---------------------

Cartwear ships with a complete set of templates (in ``cartwear/templates``).  These
will be available to an Cartwear project but can be overridden or modified.

The templates use Bootstrap conventions for class names and mark-up.

There is a separate recipe on how to do this.

Customising statics
-------------------

Cartwear's static files are stored in ``cartwear/static``.  When a Django site is
deployed, the ``collectstatic`` command is run which collects static files from
all installed apps and puts them in a single location (called the
``STATIC_ROOT``).  It is common for a separate HTTP server (like nginx) to be
used to serve these files, setting its document root to ``STATIC_ROOT``.

For an individual project, you may want to override Cartwear's static files.  The
best way to do this is to have a statics folder within your project and to add
it to the ``STATICFILES_DIRS`` setting.  Then, any files which match the same
path as files in Cartwear will be served from your local statics folder instead.
For instance, if you want to use a local version of ``cartwear/css/styles.css``,
your could create a file::

    yourproject/
        static/
            cartwear/
                css/
                    styles.css

and this would override Cartwear's equivalent file.

To make things easier, Cartwear ships with a management command for creating a copy
of all of its static files.  This breaks the link with Cartwear's static files and
means everything is within the control of the project.  Run it as follows::

    ./manage.py cartwear_fork_statics

This is the recommended approach for non-trivial projects.

Another option is simply to ignore all of Cartwear's CSS and write your own from
scratch.  To do this, you simply need to adjust the layout templates to include
your own CSS instead of Cartwear's.  For instance, you might override ``base.html``
and replace the 'less' block::

    # project/base.html

    {% block less %}
        <link rel="stylesheet" type="text/less" href="{{ STATIC_URL }}myproject/less/styles.less" />
    {% endblock %}
