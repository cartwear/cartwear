=====================
Sample Cartwear projects
=====================

Cartwear ships with three sample projects: a 'sandbox' site, which is a vanilla
install of Cartwear using the default templates and styles, a sample US site which
customises Cartwear to use US style taxes, and a fully featured
'demo' site which demonstrates how Cartwear can be re-skinned and customised to
model a domain.

The sandbox site
----------------

The sandbox site is a minimal implementation of Cartwear where everything is left
in its default state.  It is useful for exploring Cartwear's functionality
and developing new features.

It only has one notable customisation on top of Cartwear's core:

* A profile class is specified which defines a few simple fields.  This is to
  demonstrate the account section of Cartwear, which introspects the profile class
  to build a combined User and Profile form.

Note that some things are deliberately not implemented within core Cartwear as they
are domain-specific.  For instance:

* All tax is set to zero.
* No shipping methods are specified.  The default is free shipping which will
  be automatically selected during checkout (as it's the only option).
* No payment is required to submit an order as part of the checkout process.

The sandbox is, in effect, the blank canvas upon which you can build your site.

Browse the external sandbox site
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

An instance of the sandbox site is build hourly from master branch and made
available at http://latest.cartwearcommerce.com 

.. warning::
    
    It is possible for users to access the dashboard and edit the site content.
    Hence, the data can get quite messy.  It is periodically cleaned up.


Run the sandbox locally
~~~~~~~~~~~~~~~~~~~~~~~

It's pretty straightforward to get the sandbox site running locally so you can
play around with Cartwear.

.. warning::
    
    While installing Cartwear is straightforward, some of Cartwear's dependencies
    don't support Windows and are tricky to be properly installed, and therefore
    you might encounter some errors that prevent a successful installation.
    
Install Cartwear and its dependencies within a virtualenv:

.. code-block:: bash

    $ git clone https://github.com/django-cartwear/django-cartwear.git
    $ cd django-cartwear
    $ mkvirtualenv cartwear  # needs virtualenvwrapper
    (cartwear) $ make sandbox
    (cartwear) $ sites/sandbox/manage.py runserver

.. warning::
    
    Note, these instructions will install the head of Cartwear's 'master' branch,
    not an official release. Occasionally the sandbox installation process
    breaks while support for a new version of Django is being added (often due
    dependency conflicts with 3rd party libraries). Please ask on the mailing
    list if you have problems.

If you do not have ``mkvirtualenv``, then replace that line with:

.. code-block:: bash

    $ virtualenv cartwear
    $ source ./cartwear/bin/activate
    (cartwear) $

The sandbox site (initialised with a sample set of products) will be available
at: http://localhost:8000.  A sample superuser is installed with credentials::

    username: superuser
    email: superuser@example.com
    password: testing

.. _us_site:

The US site
-----------

The US site is a relatively simple Cartwear that makes a few key customisations in
order to mimic how sites in the US work. Specifically, it:

- Overrides the partner app to supply a new strategy selector which ensures all
  prices are return without taxes.

- Overrides the checkout app in order to apply taxes to submissions once the
  shipping address is known.

To browse the US site locally run:

.. code-block:: bash

   (cartwear) $ make us_site
   (cartwear) $ sites/us/manage.py runserver

and the US site will be browsable at http://localhost:8000

The demo site
-------------

The demo site is *the* reference Cartwear project as it illustrates how Cartwear can
be redesigned and customised to build an realistic e-commerce store. The demo
site is a sailing store selling a range of different product types.

The customisations on top of core Cartwear include:

* A new skin
* A variety of product types including books, clothing and downloads
* Payment with PayPal Express using django-cartwear-paypal_.
* Payment with bankcards using Datacash using django-cartwear-datacash_.

.. _django-cartwear-paypal: https://github.com/django-cartwear/django-cartwear-paypal
.. _django-cartwear-datacash: https://github.com/django-cartwear/django-cartwear-datacash

.. note::

    Both the sandbox and demo site have the Django admin interface wired up.
    This is done as a convenience for developers to browse the model instances.

    Having said that, the Django admin interface is *unsupported* and will fail
    or be of little use for some models. At the time of writing, editing
    products in the admin is clunky and slow, and editing categories is
    not supported at all.

Browse the external demo site
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

An instance of the demo site is built periodically (but not automatically) and
available at http://demo.cartwearcommerce.com. It is typically updated when new
versions of Cartwear are released.

Run the demo site locally
~~~~~~~~~~~~~~~~~~~~~~~~~

Assuming you've already set-up the sandbox site, there are two further services
required to run the demo site:

* A spatially aware database such as PostGIS.  The demo site uses
  django-cartwear-stores_ which requires a spatial capabilities for store searching.

* A search backend that supports faceting such as Solr.  You should use the
  sample schema file from ``sites/demo/deploy/solr/schema.xml``.

Once you have set up these services, create a local settings file from a template
to house your credentials:

.. code-block:: bash
    
    (cartwear) $ cp sites/demo/settings_local{.sample,}.py
    (cartwear) $ vim sites/demo/settings_local.py  # Add DB creds

Now build the demo site:

.. code-block:: bash

    (cartwear) $ make demo
    (cartwear) $ sites/demo/manage.py runserver

The demo (initialised with a sample set of products) will be available
at: http://localhost:8000.

.. _django-cartwear-stores: https://github.com/django-cartwear/django-cartwear-stores
