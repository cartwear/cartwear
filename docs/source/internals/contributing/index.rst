=====================
Contributing to Cartwear
=====================

You're thinking of helping out. That's brilliant - thank you for your time! You
can contribute in many ways:

* Join the `django-cartwear`_ mailing list and answer questions.

* :doc:`Report bugs <bugs-and-features>` in our `ticket tracker`_.

* :doc:`Submit pull requests <submitting-pull-requests>` for new and/or
  fixed behavior.

* :doc:`Improve the documentation <writing-documentation>`.

* :doc:`Write tests <running-tests>`.

* Translations can be contributed using Transifex_. Just apply for a language
  and go ahead!

.. _django-cartwear: http://groups.google.com/group/django-cartwear
.. _ticket tracker: https://github.com/django-cartwear/django-cartwear/issues
.. _Transifex: https://www.transifex.com/projects/p/django-cartwear/

Overview
--------

.. toctree::
   :maxdepth: 1
   
   development-environment
   bugs-and-features
   coding-style
   submitting-pull-requests
   running-tests
   writing-documentation
