=========
Catalogue
=========

This is an essential Cartwear app which exposes functionality to manage your
product catalogue.
:class:`cartwear.apps.catalogue.abstract_models.AbstractProduct`
is its main model.
The catalogue app also includes views specific to viewing a list or
individual products.

Abstract models
---------------

.. automodule:: cartwear.apps.catalogue.abstract_models
    :members:

Views
-----
.. automodule:: cartwear.apps.catalogue.views
    :members:
