======
Basket
======

The basket app handles shopping baskets, which essentially are a collection of
products that hopefully end up being ordered.

Abstract models
---------------

.. automodule:: cartwear.apps.basket.abstract_models
    :members:

Views
-----

.. automodule:: cartwear.apps.basket.views
    :members:
