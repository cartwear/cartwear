========
Shipping
========

See :doc:`/howto/how_to_configure_shipping` for details on how shipping works
in Cartwear.

Methods
-------

.. automodule:: cartwear.apps.shipping.methods
    :members:

Models
------

.. automodule:: cartwear.apps.shipping.models
    :members:

Repository
----------

.. automodule:: cartwear.apps.shipping.repository
    :members:
