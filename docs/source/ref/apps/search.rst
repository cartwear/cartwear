======
Search
======

Cartwear provides a search view that extends Haystack's ``FacetedSearchView`` to
provide better support for faceting.  

* Facets are configured using the ``CARTWEAR_SEARCH_FACETS`` setting, which is
  used to configure the ``SearchQuerySet`` instance within the search
  application class.

* A simple search form is injected into each template context using a context
  processor ``cartwear.apps.search.context_processors.search_form``.

Views
-----

.. automodule:: cartwear.apps.search.views
    :members:

Forms
-----

.. automodule:: cartwear.apps.search.forms
    :members:

Utils
-----

.. automodule:: cartwear.apps.search.facets
    :members:
