========
Customer
========

The customer app bundles communication with customers. This includes models
to record product alerts and sent emails. It also contains the views that
allow a customer to manage their data (profile information, shipping addresses,
etc.)

Abstract models
---------------

.. automodule:: cartwear.apps.customer.abstract_models
    :members:

Forms
-----

.. automodule:: cartwear.apps.customer.forms
    :members:

Views
-----

.. automodule:: cartwear.apps.customer.views
    :members:
