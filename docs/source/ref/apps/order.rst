=====
Order
=====

The order app handles processing of orders.

Abstract models
---------------

.. automodule:: cartwear.apps.order.abstract_models
    :members:
    :noindex:

Order processing
----------------

.. automodule:: cartwear.apps.order.processing
    :members:

Utils
-----

.. automodule:: cartwear.apps.order.utils
    :members:
