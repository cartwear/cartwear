=======
Partner
=======

The partner app mostly provides three abstract models.
:class:`cartwear.apps.partner.abstract_models.AbstractPartner` and
:class:`cartwear.apps.partner.abstract_models.AbstractStockRecord` are essential
parts of Cartwear's catalogue management.

Abstract models
---------------

.. automodule:: cartwear.apps.partner.abstract_models
    :members:

Strategy classes
----------------

.. automodule:: cartwear.apps.partner.strategy
    :members:

Pricing policies
----------------

.. automodule:: cartwear.apps.partner.prices
    :members:

Availability policies
---------------------

.. automodule:: cartwear.apps.partner.availability
    :members:
