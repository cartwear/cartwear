==================
Core functionality
==================

This page details the core classes and functions that Cartwear uses.  These aren't
specific to one particular app, but are used throughout Cartwear's codebase.

Dynamic class loading
---------------------

The key to Cartwear's flexibility is dynamically loading classes.  This allows
projects to provide their own versions of classes that extend and override the
core functionality.

.. automodule:: cartwear.core.loading
    :members: get_classes, get_class

URL patterns and views 
----------------------

Cartwear's app organise their URLs and associated views using a "Application"
class instance.  This works in a similar way to Django's admin app, and allows
Cartwear projects to subclass and customised URLs and views.

.. automodule:: cartwear.core.application
    :members:

Prices
------

Cartwear uses a custom price object for easier tax handling.  

.. automodule:: cartwear.core.prices
    :members: Price

Custom model fields
-------------------

Cartwear uses a few custom model fields.

.. automodule:: cartwear.models.fields
    :members: 
