=====================
Contributing to Cartwear
=====================

Thank you for wanting to contribute! You can find detailed guidance in the repository at ``docs/source/internals/contributing``, or online at:

http://django-cartwear.readthedocs.org/en/latest/internals/contributing/index.html
