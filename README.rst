.. image:: https://bitbucket.org/cartwear/cartwear/raw/master/docs/images/logos/cartwear.png
    :target: http://cartwear.com

=====================================================
Cartwear is a marketplace solution for niche players.
=====================================================


Cartwear is an e-commerce framework for Django designed for building domain-driven
sites.  It is structured such that any part of the core functionality can be
customised to suit the needs of your project.  This allows a wide range of
e-commerce requirements to be handled, from large-scale B2C sites to complex B2B
sites rich in domain-specific business logic.

Official homepage: http://cartwear.com

Core team:

- Ray Ch
